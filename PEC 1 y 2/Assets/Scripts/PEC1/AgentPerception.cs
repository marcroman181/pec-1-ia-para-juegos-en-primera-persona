﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentPerception : MonoBehaviour
{
    public int AngleVision = 15;
    public float MaxDetectionDistance = 5f;

    private bool Alert = false;
    private GameObject AlertExclamation;

    void Start()
    {
        this.AlertExclamation = gameObject.transform.Find("Exclamation").gameObject;
    }

    void Update()
    {
        this.DetectPlayer();
        this.AlertExclamation.SetActive(this.Alert);
    }

    private void DetectPlayer()
    {
        bool PlayerDetected = false;
        for (int index = -AngleVision; index < AngleVision; index = index + 2)
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, Quaternion.AngleAxis(index, Vector3.up) * transform.forward, out hit, MaxDetectionDistance))
            {
                Debug.DrawRay(transform.position, Quaternion.AngleAxis(index, Vector3.up) * transform.forward * hit.distance, Color.yellow);
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    PlayerDetected = true;
                    Debug.Log("Did Hit");
                }
            }
            else
            {
                Debug.DrawRay(transform.position, Quaternion.AngleAxis(index, Vector3.up) * transform.forward * MaxDetectionDistance, Color.white);
                Debug.Log("Did not Hit");
            }
            this.Alert = PlayerDetected;
        }
    }
}
