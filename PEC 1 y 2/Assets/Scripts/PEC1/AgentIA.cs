﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentIA : MonoBehaviour
{
    [HideInInspector] public WanderState wanderState;
    [HideInInspector] public PersecutionState persecutionState;
    [HideInInspector] public EscapeState escapeState;
    [HideInInspector] public AgentState currentState;

    [HideInInspector] public UnityEngine.AI.NavMeshAgent navMeshAgent;
    [HideInInspector] public Transform[] waypoints;
    [HideInInspector] public Transform agentTransform;  //Posicion del agente
    [HideInInspector] public Vector3 lastAgentPosition; //Ultima posicion detectada

    public int AngleVision = 15;                //Angulo de detección del agente
    public float MaxDetectionDistance = 5f;     //Distancia de detección del agente
    public Transform waypointsContainer;        //Lista de posiciones
    public float PersecutionDistance = 2f;      //Distancia de persecución
    public float EscapeDistance = 15f;          //Distancia de escape

    private GameObject AlertExclamation;

    void Start()
    {
        wanderState = new WanderState(this);
        persecutionState = new PersecutionState(this);
        escapeState = new EscapeState(this);
        this.currentState = wanderState;

        navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        this.AlertExclamation = gameObject.transform.Find("Exclamation").gameObject;
        this.waypoints = this.waypointsContainer.GetComponentsInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        currentState.UpdateState();
    }
    public bool Detect()
    {
        bool AgentDetected = false;
        for (int index = -this.AngleVision; index < this.AngleVision; index = index + 2)
        {
            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, Quaternion.AngleAxis(index, Vector3.up) * this.transform.forward, out hit, this.MaxDetectionDistance))
            {
                Debug.DrawRay(this.transform.position, Quaternion.AngleAxis(index, Vector3.up) * this.transform.forward * hit.distance, Color.yellow);
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    agentTransform = hit.collider.gameObject.transform;
                    AgentDetected = true;
                }
            }
            else
            {
                Debug.DrawRay(this.transform.position, Quaternion.AngleAxis(index, Vector3.up) * this.transform.forward * this.MaxDetectionDistance, Color.white);
            }
        }
        return AgentDetected;
    }

    public void PaintAlertExclamation(Color color)
    {
        MeshRenderer[] renderers = this.AlertExclamation.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = color;
        }
        this.AlertExclamation.GetComponent<Light>().color = color;
    }
    public void SetActiveAlertExclamation(bool active)
    {
        this.AlertExclamation.SetActive(active);
    }
}
