﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderState : AgentState
{
    AgentIA myAgent;

    public WanderState(AgentIA agentIA)
    {
        this.myAgent = agentIA;
    }

    public void UpdateState()
    {
        WanderMovement();
        DetectPlayer();
    }

    public void GoToWanderState()
    {
        
    }

    public void GoToPersecutionState()
    {
        Debug.Log("Wander -> Persecution");
        myAgent.SetActiveAlertExclamation(true);
        myAgent.currentState = myAgent.persecutionState;
    }

    public void GoToEscapeState()
    {
    }

    public void DetectPlayer()
    {
        if(myAgent.Detect())
        {
            GoToPersecutionState();
        }
    }

    private void WanderMovement()
    {
        if (!myAgent.navMeshAgent.pathPending)
        {
            if (myAgent.navMeshAgent.remainingDistance <= myAgent.navMeshAgent.stoppingDistance)
            {
                if (!myAgent.navMeshAgent.hasPath || myAgent.navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    int randomPosition = Random.Range(0, myAgent.waypoints.Length - 1);
                    myAgent.navMeshAgent.destination = myAgent.waypoints[randomPosition].position;
                }
            }
        }
    }
}
