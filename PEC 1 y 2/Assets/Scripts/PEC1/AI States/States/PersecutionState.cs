﻿using UnityEngine;
using System.Collections;

public class PersecutionState : AgentState
{
    AgentIA myAgent;

    private Color PersecutionColor = Color.red;
    private Color ReturnColor = Color.green;

    public PersecutionState(AgentIA agentIA)
    {
        this.myAgent = agentIA;
    }
    public void UpdateState()
    {
        if(myAgent.agentTransform != null)
        {
            Persecute();
        } else
        {
            GoToLastPosition();
            myAgent.Detect();
        }
    }

    public void GoToWanderState()
    {
        Debug.Log("Persecution -> Wander");
        myAgent.SetActiveAlertExclamation(false);
        myAgent.currentState = myAgent.wanderState;
    }

    public void GoToPersecutionState()
    {
    }

    public void GoToEscapeState()
    {
        Debug.Log("Persecution -> Escape");
        myAgent.lastAgentPosition = myAgent.agentTransform.position;
        Vector3 escapeDirection = myAgent.transform.position - myAgent.lastAgentPosition;

        myAgent.navMeshAgent.destination = escapeDirection * myAgent.EscapeDistance;
        myAgent.agentTransform = null;
        myAgent.currentState = myAgent.escapeState;
    }

    private void Persecute()
    {
        myAgent.PaintAlertExclamation(PersecutionColor);
        myAgent.navMeshAgent.destination = myAgent.agentTransform.position;
        if (myAgent.navMeshAgent.remainingDistance <= myAgent.PersecutionDistance)
        {
            GoToEscapeState();
        }
        
    }

    private void GoToLastPosition()
    {
        myAgent.PaintAlertExclamation(ReturnColor);
        myAgent.navMeshAgent.destination = myAgent.lastAgentPosition;
        if (!myAgent.navMeshAgent.pathPending)
        {
            if (myAgent.navMeshAgent.remainingDistance <= myAgent.navMeshAgent.stoppingDistance)
            {
                if (!myAgent.navMeshAgent.hasPath || myAgent.navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    GoToWanderState();
                }
            }
        }
    }
}

