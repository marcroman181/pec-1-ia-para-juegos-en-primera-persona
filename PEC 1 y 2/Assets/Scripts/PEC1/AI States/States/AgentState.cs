﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface AgentState
{
    void UpdateState();
    void GoToWanderState();
    void GoToPersecutionState();
    void GoToEscapeState();
}