﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscapeState : AgentState
{
    AgentIA myAgent;
    private Color EscapeColor = Color.yellow;


    public EscapeState(AgentIA agentIA)
    {
        this.myAgent = agentIA;
    }
    public void UpdateState()
    {
        Escape();
    }

    public void GoToWanderState() 
    {

    }

    public void GoToPersecutionState()
    {
        Debug.Log("Escape -> Persecution");
        myAgent.currentState = myAgent.persecutionState;
    }

    public void GoToEscapeState()
    {
    }
    private void Escape()
    {
        myAgent.PaintAlertExclamation(EscapeColor);
        if (!myAgent.navMeshAgent.pathPending)
        {
            if (myAgent.navMeshAgent.remainingDistance <= myAgent.navMeshAgent.stoppingDistance)
            {
                if (!myAgent.navMeshAgent.hasPath || myAgent.navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    GoToPersecutionState();
                }
            }
        }
    }
}
