﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class FollowPlayer : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform playerPosition;

    void Update()
    {
       agent.destination = playerPosition.position;
    }
}