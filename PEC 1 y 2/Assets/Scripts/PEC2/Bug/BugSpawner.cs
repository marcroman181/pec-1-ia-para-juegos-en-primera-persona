﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugSpawner : MonoBehaviour {

    public GameObject prefab;
    public float spawnRadius = 10;
    public int spawnCount = 10;

    void Awake()
    {
        for (int i = 0; i < spawnCount; i++)
        {
            Vector3 pos = transform.position + Random.insideUnitSphere * spawnRadius;
            GameObject bug = Instantiate(prefab);
            bug.transform.position = pos;
            bug.transform.forward = Random.insideUnitSphere;
        }
    }
}

