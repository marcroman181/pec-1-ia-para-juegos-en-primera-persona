﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugsManager : MonoBehaviour
{

    public BugSettings settings;

    List<Bug> bugs = new List<Bug>();

    void Start()
    {
        bugs.AddRange(FindObjectsOfType<Bug>());
        foreach (Bug b in bugs)
        {
            b.Initialize(settings);
        }

    }
    void Update()
    {
        if (bugs != null)
        {

            for (int i = 0; i < bugs.Count; i++)
            {
                BugData bugData = ResolveBugData(i);

                bugs[i].avgFlockHeading = bugData.flockHeading;
                bugs[i].centreOfFlockmates = bugData.flockCentre;
                bugs[i].avgAvoidanceHeading = bugData.avoidanceHeading;
                bugs[i].numPerceivedFlockmates = bugData.numFlockmates;

                bugs[i].UpdateBug();
            }
        }
    }


    private BugData ResolveBugData(int i)
    {
        BugData BugData = new BugData
        {
            avoidanceHeading = Vector3.zero,
            flockHeading = Vector3.zero,
            numFlockmates = 0
        };

        for (int j = 0; j < bugs.Count; j++)
        {
            if (i != j)
            {
                float distance = Vector3.Distance(bugs[j].transform.position, bugs[i].transform.position);
                if (distance <= settings.perceptionRadius)
                {
                    if (distance  < settings.avoidanceRadius)
                    {
                        BugData.avoidanceHeading -= (bugs[j].transform.position - bugs[i].transform.position) / (distance * distance);
                    }
                    BugData.flockHeading += bugs[j].GetComponent<Bug>().forward;
                    BugData.numFlockmates++;
                    BugData.flockCentre += bugs[j].transform.position;
                }
            }
        }

        return BugData;
    }

    public struct BugData
    {
        public Vector3 flockHeading;
        public Vector3 flockCentre;
        public Vector3 avoidanceHeading;
        public int numFlockmates;
    }

    //PEC3
    public void RemoveBug(Bug bug)
    {
        bugs.Remove(bug);
    }


}
