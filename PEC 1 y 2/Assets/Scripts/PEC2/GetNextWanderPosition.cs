﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using UnityEngine.AI;

namespace BBUnity.Actions
{

    [Action("Vector3/GetNextWanderPosition")]
    [Help("Get next wander position")]
    public class GetNextWanderPosition : GOAction
    {
        [OutParam("nextPosition")]
        [Help("Next Position to move")]
        public Vector3 nextPosition;

        [InParam("wanderRadius")]
        [Help("Distance to obtain wander position")]
        public float wanderRadius;
        
        public override void OnStart()
        {
            Vector3 randDirection = Random.insideUnitSphere * wanderRadius + gameObject.transform.position;

            NavMeshHit navHit;

            NavMesh.SamplePosition(randDirection, out navHit, wanderRadius, -1);

            nextPosition = navHit.position;
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
