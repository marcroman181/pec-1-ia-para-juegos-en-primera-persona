﻿using Pada1.BBCore.Framework;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{

    [Condition("Basic/IsHunting")]
    public class IsHunting : GOCondition
    {
		public override bool Check()
        {
            return gameObject.GetComponent<Dog>().IsHunting;
        }
    }
}
