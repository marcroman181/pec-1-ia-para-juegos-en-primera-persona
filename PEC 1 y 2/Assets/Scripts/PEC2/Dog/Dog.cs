﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : MonoBehaviour
{

    private float timer;
    public bool IsHunting;
    public bool IsKilling;
    private GameObject Exclamation;
    private UnityEngine.AI.NavMeshAgent navAgent;
    private Light light;


    void Start()
    {
        Exclamation = transform.Find("Exclamation").gameObject;
        navAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
        light = gameObject.GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(timer > 15) 
        {
            IsHunting = true;
        }

        if(IsKilling)
        {
            Exclamation.SetActive(true);
            PaintAlertExclamation(Color.red);
            light.enabled = true;
        } else if(IsHunting)
        {
            Exclamation.SetActive(true);
            navAgent.speed = 7;
            PaintAlertExclamation(Color.yellow);
        } else
        {
            navAgent.speed = 3.5f;
            Exclamation.SetActive(false);
            light.enabled = false;
        }

    }

    public void ResetTimer()
    {
        timer = 0;
    }
    public void PaintAlertExclamation(Color color)
    {
        MeshRenderer[] renderers = Exclamation.GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].material.color = color;
        }
    }
}
