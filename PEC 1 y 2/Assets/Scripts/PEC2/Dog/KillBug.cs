﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Navigation/KillBug")]
    public class KillBug : GOAction
    {
        private static float KILLING_TIME = 2f;

        [InParam("target")]
        public GameObject target;

        private float timer;

        public override void OnStart()
        {
            timer = 0;
            target.GetComponent<Bug>().Stop();
            gameObject.GetComponent<Dog>().IsKilling = true;
        }

        public override TaskStatus OnUpdate()
        {
            timer += Time.deltaTime; 
            
            if (timer > KILLING_TIME)
            {
                target.GetComponent<Bug>().Kill();
                gameObject.GetComponent<Dog>().IsHunting = false;
                gameObject.GetComponent<Dog>().IsKilling = false;
                gameObject.GetComponent<Dog>().ResetTimer();
                return TaskStatus.COMPLETED;
            }

            return TaskStatus.RUNNING;
        }
    }
}
