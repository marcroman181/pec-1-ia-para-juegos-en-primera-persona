﻿
using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using UnityEngine.AI;

namespace BBUnity.Actions
{

    [Action("GameObject/GetNearestBug")]
    [Help("Get next wander position")]
    public class GetNearestBug : GOAction
    {
        [OutParam("nearestBug")]
        public GameObject nearestBug;

        public override void OnStart()
        {
            Bug[] bugs = GameObject.FindObjectsOfType<Bug>();
            nearestBug = bugs[0].gameObject;
            float nearestBugDistance = Vector3.Distance(nearestBug.transform.position, gameObject.transform.position);
            for (int i = 0; i < bugs.Length; i++)
            {
                float distance = Vector3.Distance(bugs[i].transform.position, gameObject.transform.position);
                if(distance < nearestBugDistance)
                {
                    nearestBugDistance = distance;
                    Debug.Log(bugs[i].transform.position);
                    Debug.Log(gameObject.transform.position);
                    Debug.Log(nearestBugDistance);
                    nearestBug = bugs[1].gameObject;
                }
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}