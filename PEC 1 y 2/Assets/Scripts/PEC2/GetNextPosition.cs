﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using UnityEngine.AI;

namespace BBUnity.Actions
{

    [Action("Vector3/GetNextPosition")]
    [Help("Get next position from waypoints list")]
    public class GetNextPosition : GOAction
    {

        [InParam("waypointsContainer")]
        [Help("Waypoints container list")]
        public Transform waypointsContainer;

        [OutParam("nextPosition")]
        [Help("Next Position to move")]
        public Vector3 nextPosition;

        private Transform[] waypoints;
        public override void OnStart()
        {
            int actualPosition = gameObject.GetComponent<ActualPosition>().indexPosition;

            this.waypoints = this.waypointsContainer.GetComponentsInChildren<Transform>();
            
            actualPosition++;
            if (actualPosition == this.waypoints.Length)
            {
                actualPosition = 1;
            }
            this.nextPosition = waypoints[actualPosition].position;
            gameObject.GetComponent<ActualPosition>().indexPosition = actualPosition;
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}